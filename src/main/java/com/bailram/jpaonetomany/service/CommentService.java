package com.bailram.jpaonetomany.service;

import com.bailram.jpaonetomany.domain.dto.CommentRequest;
import com.bailram.jpaonetomany.domain.entity.Comment;
import com.bailram.jpaonetomany.exception.ResourceNotFoundException;
import com.bailram.jpaonetomany.repository.CommentRepository;
import com.bailram.jpaonetomany.repository.PostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class CommentService {
    @Autowired
    private CommentRepository commentRepository;

    @Autowired
    private PostRepository postRepository;

    public Page<Comment> getAllCommentByPostId(Long postId, Pageable pageable) {
        return commentRepository.findByPostId(postId, pageable);
    }

    public Comment createComment(Long postId, CommentRequest request) {
        return postRepository.findById(postId).map(post -> {
            Comment comment = Comment.builder()
                    .text(request.getText())
                    .post(post)
                    .build();
            return commentRepository.save(comment);
        }).orElseThrow(() -> new ResourceNotFoundException("PostId " + postId + " not found"));
    }

    public Comment updateComment(Long postId, Long commentId, CommentRequest request) {
        if (!postRepository.existsById(postId)) {
            throw new ResourceNotFoundException("Post Id " + postId + " not found");
        }

        return commentRepository.findById(commentId).map(comment -> {
            comment.setText(request.getText());
            return commentRepository.save(comment);
        }).orElseThrow(() -> new ResourceNotFoundException("Comment Id " + commentId + " not found"));
    }

    public String deleteComment(Long postId, Long commentId) {
        return commentRepository.findByIdAndPostId(commentId, postId).map(comment -> {
            commentRepository.delete(comment);
            return "Succesfully delete comment";
        }).orElseThrow(() -> new ResourceNotFoundException("Comment Id " + commentId + " not found"));
    }
}
