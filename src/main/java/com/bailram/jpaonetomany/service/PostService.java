package com.bailram.jpaonetomany.service;

import com.bailram.jpaonetomany.domain.dto.PostRequest;
import com.bailram.jpaonetomany.domain.entity.Post;
import com.bailram.jpaonetomany.exception.ResourceNotFoundException;
import com.bailram.jpaonetomany.repository.PostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class PostService {
    @Autowired
    private PostRepository postRepository;

    public Page<Post> getAllPosts(Pageable pageable) {
        return postRepository.findAll(pageable);
    }

    public Post createPost(PostRequest request) {
        Post data = Post.builder()
                .title(request.getTitle())
                .description(request.getDescription())
                .content(request.getContent())
                .build();
        return postRepository.save(data);
    }

    public Post updatePost(Long postId, PostRequest request) {
        return postRepository.findById(postId).map(data -> {
            data.setTitle(request.getTitle());
            data.setDescription(request.getDescription());
            data.setContent(request.getContent());
            return postRepository.save(data);
        }).orElseThrow(() -> new ResourceNotFoundException("Post Id " + postId + " not found"));
    }

    public String deletePost(Long postId) {
        return postRepository.findById(postId).map(post -> {
            postRepository.delete(post);
            return "Successfully delete Post";
        }).orElseThrow(() -> new ResourceNotFoundException("Post Id " + postId + " not found"));
    }
}
