package com.bailram.jpaonetomany.domain.dto;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PostRequest {
    @NotNull
    private String title;

    @NotNull
    private String description;

    @NotNull
    private String content;
}
