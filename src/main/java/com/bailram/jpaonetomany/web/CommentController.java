package com.bailram.jpaonetomany.web;

import com.bailram.jpaonetomany.domain.dto.CommentRequest;
import com.bailram.jpaonetomany.domain.entity.Comment;
import com.bailram.jpaonetomany.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class CommentController {
    @Autowired
    private CommentService commentService;

    @GetMapping("/posts/{postId}/comments")
    public Page<Comment> getAllCommentsByPostId(@PathVariable Long postId, Pageable pageable) {
        return commentService.getAllCommentByPostId(postId, pageable);
    }

    @PostMapping("/posts/{postId}/comments")
    public Comment createComment(@PathVariable Long postId, @RequestBody CommentRequest request) {
        return commentService.createComment(postId, request);
    }

    @PutMapping("/posts/{postId}/comments/{commentId}")
    public Comment updateComment(@PathVariable(value = "postId") Long postId,
                                 @PathVariable(value = "commentId") Long commentId,
                                 @RequestBody CommentRequest request) {
        return commentService.updateComment(postId, commentId, request);
    }

    @DeleteMapping("/posts/{postId}/comments/{commentId}")
    public ResponseEntity<String> deleteComment(@PathVariable(value = "postId") Long postId,
                                                @PathVariable(value = "commentId") Long commentId) {
        return new ResponseEntity<>(commentService.deleteComment(postId,commentId), HttpStatus.OK);
    }
}
