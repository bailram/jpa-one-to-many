# JPA One To Many
Result add new post <br>
![Screenshot1](screenshot/Screenshot_1.png) <br>
Result get all post with pagination <br>
![Screenshot2](screenshot/Screenshot_2.png) <br>
Result update post <br>
![Screenshot5](screenshot/Screenshot_5.png) <br>
Result delete post <br>
![Screenshot6](screenshot/Screenshot_6.png) <br>
Result add new comment <br>
![Screenshot3](screenshot/Screenshot_3.png) <br>
Result get all comment by post id with pagination <br>
![Screenshot4](screenshot/Screenshot_4.png) <br>
Result update comment by post id and comment id <br>
![Screenshot7](screenshot/Screenshot_7.png) <br>
Result delete comment by post id and comment id <br>
![Screenshot8](screenshot/Screenshot_8.png) <br>
